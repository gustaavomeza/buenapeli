


import requests
from bs4 import BeautifulSoup
from requests import get
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import sys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait as wait
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException  
import scrapy

driver = webdriver.Firefox(executable_path=r'C:\Users\geckodriver.exe')


def check_exists_by_xpath(xpath):
    try:
        driver.find_element_by_xpath(xpath)
    except NoSuchElementException:
        return False
    return True





class QuotesSpider(scrapy.Spider):


    name = 'quotes'
    start_urls = [
        'https://pelisgratis.live/',
    ]


    def parse(self, response):


        for quote in response.css('.item-article'):

            nombre_pelicula = quote.css('div > a > .title > h3::text').get()
            imagen_pelicula = quote.css('div > a > figure > img::attr(src)').get()
            fuente_url = quote.css('a::attr(href)').get()

            
        
            driver.get(fuente_url)
            time.sleep(3)
            

            video = driver.find_element_by_tag_name("iframe").get_attribute("src")
            background = driver.find_element_by_xpath("//img[1]").get_attribute("src")
            desc = driver.find_element_by_xpath("//p[@class='p-slide-slimScroll']").text
            genero = driver.find_element_by_xpath("//div[@class='generos']").text

            idioma = driver.find_element_by_xpath("//div[@class='dropdown']/button").text



            frame = driver.find_element_by_tag_name("iframe")
            driver.switch_to.frame(frame)
            num = check_exists_by_xpath("//a[@id='servers'][2]")

            if num == True:
              driver.find_element_by_xpath("//a[@id='servers'][2]").click()
              time.sleep(3)
              b = driver.find_element_by_id("bgPlayer").get_attribute("outerHTML")
              cad = b[32::]
              bus = cad.split('"')
              video = bus[0]
            
      




            yield {
                'nombre_pelicula': nombre_pelicula,
                'imagen_pelicula': imagen_pelicula,
                'fuente_url': fuente_url,
                "video_principal": video,
                "background": background,
                "descripcion_pelicula": desc,
                "onde": "2",
                "genero": genero,
                "idioma": idioma,

            }


        



#scrapy runspider peligra_ori.py -o peligra.json


