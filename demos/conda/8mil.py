


import requests
from bs4 import BeautifulSoup
import scrapy







class QuotesSpider(scrapy.Spider):
    name = 'quotes'
    start_urls = [
        'https://www.locopelis.com/',
    ]

    def parse(self, response):






        

        for next_page in response.css('.nav>li>a'):
            yield response.follow(next_page, self.parse)
        
        for quote in response.css('li.peli_bx'):
           
           


            nombre = quote.css('.plt_tit>h2::text').get()
            genero = quote.css('.plt_ft>.stars::text').get()
            desc = quote.css('div>.peli_txt>p::text').get()
            idioma = quote.css('div>.stars:last-child::text').getall()
            url = quote.css('.peli_img_img>a::attr(href)').get()
            x = genero.split(',')
            img = quote.css('.peli_img_img>a>img::attr(src)').get()
        
        

            req = requests.get(url)
            code = req.status_code
        


            if code == 200:
                soup = BeautifulSoup(req.content, "html.parser")
                descripcion = soup.strong.get_text()
                video = soup.iframe['src']
            else:
                descripcion = "none"
                video = "none"





       
    


            yield {
                'nombre': nombre,
                'genero': x[0].replace(': ',''),
                'ano': x[1],
                'mini_desc': desc,
                'desc': descripcion,
                'idioma': idioma[1],
                'url': url,
                'img': img,
                'descripcion': descripcion,
                'video': video,
                'estado':'1'
            }
        




#scrapy runspider 8mil.py -o 8mil.json
#scrapy runspider 8mil.py
