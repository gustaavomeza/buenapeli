



import requests
from bs4 import BeautifulSoup

import scrapy




class QuotesSpider(scrapy.Spider):
    name = 'quotes'
    start_urls = [
        'https://www.locopelis.com/',
    ]

    def parse(self, response):






        

        for next_page in response.css('.nav>li>a'):
            yield response.follow(next_page, self.parse)
        
        for quote in response.css('li.peli_bx'):
           
           


            nombre = quote.css('.plt_tit>h2::text').get()
            genero = quote.css('.plt_ft>.stars::text').get()
            desc = quote.css('div>.peli_txt>p::text').get()
            idioma = quote.css('div>.stars:last-child::text').getall()
            url = quote.css('.peli_img_img>a::attr(href)').get()
            x = genero.split(',')
            img = quote.css('.peli_img_img>a>img::attr(src)').get()
        
        

            req = requests.get(url)
            code = req.status_code
        


            if code == 200:
                soup = BeautifulSoup(req.content, "html.parser")
                descripcion = soup.strong.get_text()
                video = soup.iframe['src']
                estatus = "1"
            else:
                descripcion = "none"
                video = "none"
                estatus = "0"






            yield {
      
                 "nombre_pelicula" : nombre,
                 "ano_pelicula" : x[1],
                 "descripcion_pelicula" : descripcion,
                 "imagen_pelicula" : img,
                 "fuente_url" : url,
                 "video_principal" : video,
                 "idioma_video_principal" : idioma[1],
                 "estatus" : estatus,
                 "mini_descripcion" : desc,
                 "genero" : x[0].replace(': ','')
            }
        




#scrapy runspider conda_copy.py -o quotes.json
#scrapy runspider conda.py
