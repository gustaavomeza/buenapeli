

<!DOCTYPE HTML>
<html>
<head>
<title>moogi</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>





<link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/mdb.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.css" integrity="sha256-831vUCpalhrFw/F8jVhoX/aw3A9pwtzdN5zKvJbsO/I=" crossorigin="anonymous" />


<link href="https://fonts.googleapis.com/css?family=Indie+Flower" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Covered+By+Your+Grace" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Acme&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Catamaran&display=swap" rel="stylesheet">

<style type="text/css">

body {
  background: #3b434c;
}

.categoria{
  text-align: center;
  font-family: 'Indie Flower', cursive;
  letter-spacing: 1px;
  font-size: 15px;
}

.navbar form .md-form input {
    margin: 0 8px 0px 8px;
}

.modconteanmM {
    background-color: transparent !important;
}


.mopores::-webkit-input-placeholder  {
  font-family: 'Catamaran', sans-serif;
  color: #fff;
}
.mopores,.mopores:focus,.mopores:hover {
  outline: none;
  background: transparent;
  color: #fff;
  margin-bottom: 15px !important;
  border: 0px !important;
  font-size: 18px;
  box-shadow: none;
  font-family: 'Catamaran', sans-serif;
}


.mi-title {
  font-family: 'Acme', sans-serif;
}
.mi-title a {
  line-height: 1px;
  color: #000;
}
.btn-ver {
  color: #000;
  font-family: 'Acme', sans-serif;
  text-transform: uppercase;
  font-size: 15px;
  font-weight: 700;
}
.btn-ver:hover {
  color: #000;
}

.mi-icon1 {
  font-weight: 10;
  font-size: 18px;
  vertical-align: baseline;
}
.m-nav {
  margin-top: 120px;
}

.m-sec {  
  margin-top: 66px;
}

#loginModal {
  z-index: 3000;
}
.modal-backdrop {
  z-index: 2700;
}

.nav-negro-color {
  background-color: #212121 !important;
  font-family: 'Indie Flower', cursive;
  font-size: 22px;
  padding: 15px;
  z-index: 2500;
}

.form-negro > div > input {
  border: 0px !important;
  font-size: 22px;
}
.form-negro > div > input:focus {

  -webkit-box-shadow: 0px 0px 0px 0px rgba(0,0,0,0.75) !important;
  -moz-box-shadow: 0px 0px 0px 0px rgba(0,0,0,0.75) !important;
  box-shadow: 0px 0px 0px 0px rgba(0,0,0,0.75) !important;

}

.alert-ama {
    color: #212121;
    background-color: #ffe469;
    border-color: #ffe469;
    font-family: 'Indie Flower', cursive;
    border-radius: 0px;
    font-weight: 700;
}

.alert-noma{
  color: #212121;
  background-color: #ceff78;
  border-color: #ceff78;
  font-family: 'Indie Flower', cursive;
  border-radius: 0px;
  font-weight: 700;
}

.intupu::placeholder {
  color: #fff;
}

.intupu {
  color: #fff;
  margin: 0px !important;
  margin-top: 1px !important;
  border: 0px !important;
  font-size: 22px;
}
.intupu:focus {

  color: #fff;
  -webkit-box-shadow: 0px 0px 0px 0px rgba(0,0,0,0.75) !important;
  -moz-box-shadow: 0px 0px 0px 0px rgba(0,0,0,0.75) !important;
  box-shadow: 0px 0px 0px 0px rgba(0,0,0,0.75) !important;

}


.title-peli{
  color: #fff;
  font-family: 'Acme', sans-serif;
}

.descr-peli {
  color: #fff;
  font-family: 'Catamaran', sans-serif;
  font-size: 17px;
}


.popover-body {
  font-family: 'Catamaran', sans-serif;
  padding: 1.5rem;
  color: #000;
  line-height: 22px;
  font-size: 18px;
  background: #ffe469;
}

.bs-popover-auto[x-placement^=left] .arrow::after, .bs-popover-left .arrow::after {
    border-left-color: #ffe469 !important;
}


.page-item {
    margin: 0px 10px;
}

.page-item > span {
  font-family: 'Catamaran', sans-serif !important;
  padding: 0px !important;
  margin: 0px !important;
}

.page-item > span > a {
    color: #fff;
    font-size: 20px;
    margin: 0px;
    padding: 12px;
    display: block;
    width: 60px;
    height: 60px;
    text-align: center;
    justify-content: center;
    display: inline-grid;
    align-items: center;

}
.page-item > span > a:hover{
  color: #000;
}


.mifoterMoadl{
  background: #212121;
  border-top: 1px solid #000000;
  background: #212121;
  border-top: 1px solid #000000;
  padding: 7px 1rem;

}

.page-item > span > a > i {
  vertical-align: -webkit-baseline-middle;
}
.pagination.pg-blue .page-item.active .page-link, .pagination.pg-blue .page-item.active .page-link:hover {
    background-color: #ffe469;
    color: #000;
}

.active > span {
    color: #fff;
    font-size: 20px !important;
    margin: 0px !important;
    padding: 12px 19px !important;
    display: block;
    width: 60px;
    height: 60px;
    text-align: center;
    justify-content: center;
    display: inline-grid;
    align-items: center;

}


.burl {
  padding: 0px 21px !important;
  margin: 0px !important;
  font-size: 13px !important;
}


.modlogin {
  background: #212121;
  color: #ffffff;
  font-family: 'Indie Flower', cursive;
  border-bottom: 1px solid #000000;
}

.closelogin, .closelogin:focus, .closelogin:hover {
  outline: none;
  color: white;
  text-shadow: 0 1px 0 #989898;
}

.bodyloginmo {
  background: #212121;
}





@media (min-width: 1200px){

  .container {
      max-width: 1440px !important;
  }

}
</style>




<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>

</head>
<body>









<nav class="navbar navbar-expand-lg navbar-dark fixed-top nav-negro-color">
  <div class="container">
    <a class="navbar-brand" href="<?php echo base_url(); ?>">
      <img src="<?php echo base_url(); ?>assets/img/logo.png" style="width: 89px;">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
      aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="basicExampleNav">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="<?php echo base_url(); ?>">Home
            <span class="sr-only">(current)</span>
          </a>
        </li>

        <?php  if(isset($_SESSION['id'])){ ?>

          <?php  if($_SESSION['tipo']=='a'){ ?>
  

          <li class="nav-item active">
            <a class="nav-link"  href="<?php echo base_url(); ?>admin">Admin
              <span class="sr-only">(current)</span>
            </a>
          </li>

          <?php } ?>



        <?php }else{ ?>
        <li class="nav-item active" data-toggle="modal" data-target="#loginModal">
          <a class="nav-link">Login
            <span class="sr-only">(current)</span>
          </a>
        </li>
        <?php } ?>


      </ul>


      <div class="form-inline form-negro">
        <div class="md-form my-0">
          <div class="input-group">
            <input type="text" class="form-control intupu" placeholder="Busar . . . "  name="ver" 
                   value="<?php if(isset($busca) && $busca != 'no busco nada'){ echo rawurldecode($busca); } ?>">
            <div class="input-group-append">
              <button class="btn btn-warning btn-ver btn-sm burl" type="button" onClick="busca();">Buscar</button>

            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</nav>





<!-- Modal -->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="LoginModalLabe"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content modconteanmM">
      <div class="modal-header modlogin">
        <h5 class="modal-title" id="LoginModalLabe">Login</h5>
        <button type="button" class="close closelogin" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body bodyloginmo">


        <div class="p-4">
          <input type="text" class="form-control mopores" placeholder="Usuario:" name="user"/>
          <input type="password" class="form-control mopores" placeholder="Contraseña:" name="password"/>
        </div>
        <div id="respp"></div>
        
      
      </div>
      <div class="modal-footer mifoterMoadl">

        <button type="button" class="btn btn-warning rounded-0 waves-effect waves-light btn-ver" data-dismiss="modal">CANCELAR</button>
        <button type="button" class="btn btn-warning rounded-0 waves-effect waves-light btn-ver"  onClick="login();">INGRESAR</button>
      </div>
    </div>
  </div>
</div>










































<script>
function cleanString (st)
{
        var ltr = ['[àáâãä]','[èéêë]','[ìíîï]','[òóôõö]','[ùúûü]','ñ','ç','[ýÿ]','\\s|\\W|_'];
        var rpl = ['a','e','i','o','u','n','c','y',' '];
        var str = String(st.toLowerCase());
        
        for (var i = 0, c = ltr.length; i < c; i++)
        {
        	var rgx = new RegExp(ltr[i],'g');
        	str = str.replace(rgx,rpl[i]);
        };
        
        return str;
}





function busca(){

  var bus = $("input[name=ver]" ).val().trim();
  var bus = cleanString(bus);

  if(bus=='' || bus == ' '){ bus = 'no busco nada'; }

  $(location).attr('href','<?php echo base_url() ?>pelicula/'+bus );
}



function login(){

  var user = $("input[name=user]" ).val().trim();
  var password = $("input[name=password]" ).val().trim();

  if(user==''){
    $("#respp").html('<div class="alert alert-ama"><strong>Complete el campo usuario!</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
    $("input[name=user]" ).focus();
    return false;
  }
  if(password==''){
    $("input[name=password]" ).focus();
    $("#respp").html('<div class="alert alert-ama"><strong>Complete el campo password!</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
    return false;
  }

  $("#respp").html('<center><div class="spinner-border text-light" role="status"><span class="sr-only">Loading...</span></div></center>');

  const data = new FormData();
  data.append('user', user);
  data.append('password', password);

  fetch('<?php echo base_url(); ?>user/login', {
    method: 'POST',
    body: data
  })
  .then(function(response) {
    return response.text().then(function(text) {

      if(text == 'no'){
        $("#respp").html('<div class="alert alert-ama"><strong>Usuario no encontrado!</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
      }
      if(text == 'si'){
        $("#respp").html('<div class="alert alert-noma"><strong>Ingresando . . . </strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
        setTimeout(function(){ location.reload(); }, 1500);
      }
     
    
    });
  });


}



$(document).keyup(function (e) {

    if ($("input[name=ver]" ).is(":focus") && (e.keyCode == 13)) {
      busca();
    }

    if ($("input[name=user], input[name=password]").is(":focus") && (e.keyCode == 13)) {
      login(1);
    }

});





</script>



































<!--
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
 
      <a href="<?php echo base_url(); ?>" ><img src="<?php echo base_url(); ?>assets/images/logo.png" alt="" style="padding: 11px;"/></a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">Inicio</a></li>
   

      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Page 1
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="#">Page 1-1</a></li>
          <li><a href="#">Page 1-2</a></li>
          <li><a href="#">Page 1-3</a></li>
        </ul>
      </li>
      <li><a href="#">Page 2</a></li>
     

    </ul>
    <form class="navbar-form navbar-left">
      <div class="form-group">
        <input type="text" class="form-control" placeholder="Buscar">
      </div>
      <button type="submit" class="btn btn-default"><i class="fa fa-search" aria-hidden="true"></i></button>
    </form>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="<?php echo base_url(); ?>dp"><i class="fa fa-tachometer" aria-hidden="true"></i> Admin</a></li>
    </ul>
  </div>
</nav>
-->