<style>
    body {
        overflow: hidden !important;
    }


    .mivideo {
        width: 220px;
        height: 97px;
        border: 0px;
    }

    .imageprop {
        max-width: 60px; 
    }

    table.dataTable thead th, table.dataTable thead td {
        font-size: 20px;

    }  
    table.dataTable tbody th, table.dataTable tbody td {
        font-size: 20px;

    }

    #example_info {
        color: #fff;
        font-family: 'Indie Flower', cursive;
        font-size: 20px;
    }
    
    .dataTables_wrapper .dataTables_paginate .paginate_button {
        color: #fff !important;
        font-family: 'Indie Flower', cursive;
        font-size: 20px;
        border: 0px !important;
        outline: none !important;
    }

    .dataTables_wrapper .dataTables_length, .dataTables_wrapper .dataTables_filter, .dataTables_wrapper .dataTables_info, .dataTables_wrapper .dataTables_processing, .dataTables_wrapper .dataTables_paginate {
        color: #fff !important;
        font-family: 'Indie Flower', cursive;
        font-size: 20px;
    }
    
    .dataTables_wrapper .dataTables_paginate .paginate_button.disabled, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:active {
        color: #fff !important;
        font-family: 'Indie Flower', cursive;
        font-size: 20px;;
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button:active, .dataTables_wrapper .dataTables_paginate .paginate_button:focus {
        outline: none !important;
        color: #212121 !important;
        border: 0px !important;
        background: transparent !important;
    }
    .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
        outline: none !important;
        color: #fff !important;
        border: 0px !important;
        background: transparent !important;
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
        outline: none !important;
        color: #212121 !important;
        border: 0px !important;
        background: #ffe469 !important;
    }

</style>

<div class="page-loader">
    <div class="loader">
        Cargando . . .
    </div>
</div>



<div class="container">
    <div class="row">




        <div class="col-md-12 mt-3">
            <div class="card rounded-0">

                <div class="card-header micardhed">
                    PELICULAS DE <?php echo strtoupper($page); ?>
                    <button class="btn btn-warning rounded-0  btn-sm btn-ver pull-right" id="elmiselect">eliminar seleccion</button>
                </div>
                <div class="card-body micardboyo">


                    <table id="example" style="width:100%">
                        <thead class="mitabb">
                            <tr>
                                <th><input type="checkbox" id="selectall"/></th>
                                <th>#</th>
                                <th>Portada</th>
                                <th>Nombre de pelicula</th>
                                <th>Video</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php
                            $i = 0;
                            foreach ($peliculas as $k => $v) {
                                $nombre_am = str_replace(" ", "-", $v->nombre_pelicula);
                                $nombre_am = str_replace(",", "", $nombre_am);
                                $nombre_am = mb_strtolower($nombre_am);
                                $nombre_am = gen_url($nombre_am);
                                $i++;
                            ?>

                                <tr id="ABUE<?php echo $i; ?>">
                                    <td><input type="checkbox" value="<?php echo $v->id_pelicula; ?>"/></td>
                                    <td><?php echo $i; ?></td>
                                    <td><img src="<?php echo $v->imagen_pelicula; ?>" class="imageprop"></td>
                                    <td><?php echo $v->nombre_pelicula; ?></td>
                                    <td><iframe src="<?php echo $v->video_principal; ?>" class="mivideo" allowfullscreen=""></iframe></td>
                                    <td>
                                        <button class="btn btn-warning rounded-0  btn-sm btn-ver" onClick="borrarPeli('<?php echo $v->id_pelicula; ?>','SEC<?php echo $i; ?>','ABUE<?php echo $i; ?>');">enviar a peliculas</button>
                                        <button class="btn btn-warning rounded-0  btn-sm btn-ver" onClick="borrarPeli('<?php echo $v->id_pelicula; ?>','SEC<?php echo $i; ?>','ABUE<?php echo $i; ?>');">eliminar</button>
                                    </td>

                                </tr>


                            <?php
                            }
                            ?>

                        </tbody>
                    </table>



                </div>
            </div>
        </div>





    </div>
</div>












<script>




    $('#selectall').on('click', function(){
        $('*td > input').prop('checked', this.checked);
    });


    $('#elmiselect').on('click', function(){
        event.preventDefault();
        var table = $('#example').DataTable();

        arr = [];
        $('*td > input').each(function(){
            if(this.checked==true){
                var x = $(this).parent();
                var padre = x[0].parentElement;
                var idpa = $(padre).attr("id");

                console.log(idpa);

                table.row("#" + idpa).remove();
                $("#" + idpa).remove();
                arr.push(this.value);
            }
        });


        $.ajax({
            url : '<?php echo base_url(); ?>admin/crud/delmas',
            data : { pelis : arr, tabla: '<?php echo $page; ?>' },
            type : 'GET',
            dataType : 'json'
        });

        $('#selectall').filter(':checkbox').prop('checked',false);




    });



    function borrarPeli(id, padre, abue) {

        
        $("#ID" + id).remove(); 
        $.ajax({
            url : '<?php echo base_url(); ?>admin/crud/del',
            data : { id_pelicula : id, tabla: '<?php echo $page; ?>' },
            type : 'GET',
            dataType : 'json'
        }); 

        var table = $('#example').DataTable();
        table.row("#" + abue).remove();
        $("#" + abue).remove();
        

    }


    $(document).ready(function() {

        

        $(".page-loader").remove();
        $("#war").css("display", "block");
        $('html, body').css('overflow', 'auto');

        $('#example').DataTable({
            "language": {
                "lengthMenu": "Muestrame _MENU_ items",
                "zeroRecords": "No se encontro nada disculpe.",
                "info": "Mostrando _PAGE_ de _PAGES_",
                "infoEmpty": "No encontramos nada",
                "infoFiltered": "(_MAX_)"
            },
            
            columnDefs: [ 
                {
                orderable: false,
                className: 'select-checkbox',
                targets:0
                }
            ],
            select: {
                style:    'os',
                selector: 'td:first-child'
            }


        });



  










    
    });
</script>