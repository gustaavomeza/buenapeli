<style>
    body {
        overflow: hidden !important;
    }


    .mivideo {
        width: 220px;
        height: 97px;
        border: 0px;
    }

    .imageprop {
        max-width: 60px; 
    }

    table.dataTable thead th, table.dataTable thead td {
        font-size: 20px;

    }  
    table.dataTable tbody th, table.dataTable tbody td {
        font-size: 20px;

    }

    #example_info {
        color: #fff;
        font-family: 'Indie Flower', cursive;
        font-size: 20px;
    }
    
    .dataTables_wrapper .dataTables_paginate .paginate_button {
        color: #fff !important;
        font-family: 'Indie Flower', cursive;
        font-size: 20px;
        border: 0px !important;
        outline: none !important;
    }

    .dataTables_wrapper .dataTables_length, .dataTables_wrapper .dataTables_filter, .dataTables_wrapper .dataTables_info, .dataTables_wrapper .dataTables_processing, .dataTables_wrapper .dataTables_paginate {
        color: #fff !important;
        font-family: 'Indie Flower', cursive;
        font-size: 20px;
    }
    
    .dataTables_wrapper .dataTables_paginate .paginate_button.disabled, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:active {
        color: #fff !important;
        font-family: 'Indie Flower', cursive;
        font-size: 20px;;
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button:active, .dataTables_wrapper .dataTables_paginate .paginate_button:focus {
        outline: none !important;
        color: #212121 !important;
        border: 0px !important;
        background: transparent !important;
    }
    .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
        outline: none !important;
        color: #fff !important;
        border: 0px !important;
        background: transparent !important;
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
        outline: none !important;
        color: #212121 !important;
        border: 0px !important;
        background: #ffe469 !important;
    }

</style>

<div class="page-loader">
    <div class="loader">
        Cargando . . .
    </div>
</div>



<div class="container">
    <div class="row">

    



        <div class="col-md-12 mt-3">
            <div class="card rounded-0">

                <div class="card-header micardhed">PELICULAS DE PELICULAS PRINCIPALES</div>
                <div class="card-body micardboyo">


                    <table id="example" style="width:100%">
                        <thead class="mitabb">
                            <tr>
                                <th>#</th>
                                <th>Portada</th>
                                <th>Nombre de pelicula</th>
                                <th>Video</th>
                                <th>Pelicula link</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php
                            $i = 0;
                            foreach ($peliculas as $k => $v) {
                                $nombre_am = str_replace(" ", "-", $v->nombre_pelicula);
                                $nombre_am = str_replace(",", "", $nombre_am);
                                $nombre_am = mb_strtolower($nombre_am);
                                $nombre_am = gen_url($nombre_am);
                                $i++;
                            ?>

                                <tr id="ABUE<?php echo $i; ?>">
                                    <td><?php echo $i; ?></td>
                                    <td><img src="<?php echo $v->imagen_pelicula; ?>" class="imageprop"></td>
                                    <td><?php echo $v->nombre_pelicula; ?></td>
                                    <td><iframe src="<?php echo $v->video_principal; ?>" class="mivideo" allowfullscreen=""></iframe></td>
                                    <td><a href="<?php echo base_url() . 'verpeli/' . $nombre_am . '/' . $v->id_pelicula; ?>" target="_blank" class="btn btn-warning rounded-0  btn-sm  btn-ver">ver pelicula</a></td>
                                    <td><button class="btn btn-warning rounded-0  btn-sm btn-ver" 
                                                        onClick="borrarPeli('<?php echo $v->id_pelicula; ?>','SEC<?php echo $i; ?>','ABUE<?php echo $i; ?>');">eliminar</button></td>

                                </tr>


                            <?php
                            }
                            ?>

                        </tbody>
                    </table>



                </div>
            </div>
        </div>










        <!--<div class="col-3 categorias">
          <div class="alert alert-ama">C A T E G O R I A S :</div>
          <div class="row m-0">
          </div>
        </div>
        <div class="col-9">
             <div class="row">dfgfgdfg</div>
        </div> -->


    </div>
</div>












<script>
    function borrarPeli(id, padre, abue) {

        
        $("#ID" + id).remove(); 
        $.ajax({
            url : '<?php echo base_url(); ?>admin/crud/del',
            data : { id_pelicula : id },
            type : 'GET',
            dataType : 'json'
        }); 

        var table = $('#example').DataTable();
        table.row("#" + abue).remove();
        $("#" + abue).remove();
        

    }


    $(document).ready(function() {


        $(".page-loader").remove();
        $('html, body').css('overflow', 'auto');

        $('#example').DataTable({
            "language": {
                "lengthMenu": "Muestrame _MENU_ items",
                "zeroRecords": "No se encontro nada disculpe.",
                "info": "Mostrando _PAGE_ de _PAGES_",
                "infoEmpty": "No encontramos nada",
                "infoFiltered": "(_MAX_)"
            }
        });



  










    
    });
</script>