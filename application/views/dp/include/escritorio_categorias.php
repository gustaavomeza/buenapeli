  


  <div class="col-md-2">
    <div class="panel panel-default">
      <div class="panel-heading"><strong>OPCIONES</strong></div>

   
     
      <div class="list-group">

          <a href="<?php echo base_url(); ?>dp/adm/categorias" class="list-group-item">
            CATEGORIAS
            <span class="badge badge-default badge-pill" id="cant_categorias"><?php echo $numero_categorias; ?></span>
          </a>
          <a href="<?php echo base_url(); ?>dp/adm/peliculas" class="list-group-item">
            PELICULAS
            <span class="badge badge-default badge-pill"><?php echo $numero_peliculas; ?></span>
          </a>

      </div>

    </div>
  </div>

  <div class="col-md-5">


    

    <div class="panel panel-default">
    
      <div class="panel-heading"><strong>EDITAR CATEGORIAS</strong></div>
    
    
      <ul class="list-group" id="padre_categias">

        <?php foreach($lista as $fila) : 
        echo '
          <li class="list-group-item clearfix" id="categoria_'.$fila->id_categoria.'">
            <span id="nombre_categoria_'.$fila->id_categoria.'">'.$fila->nombre_categoria.'</span>
            <div class="btn-group pull-right">
              <button type="button" class="btn btn-default" onClick="editarCAT('.$fila->id_categoria.');">
                <i class="fa fa-pencil" aria-hidden="true"></i>
              </button>
              <button type="button" class="btn btn-default" onClick="elimiaCAT('.$fila->id_categoria.');">
                <i class="fa fa-trash-o" aria-hidden="true"></i>
              </button>
            </div>
          </li>
        ';

        endforeach;
        if ($numero_categorias < 1) {
          echo '
          <li class="list-group-item clearfix">No existen categorias</li>
          ';
        }
        ?>
        
      </ul>
      <!--<div class="panel-body"> </div>-->
      <div class="panel-footer">
        <?php echo $paginacion ?>
      </div>



      

    </div>
    <script type="text/javascript">

      function elimiaCAT($idCat) {
        parametros = {
          "idcat" : $idCat
        };
        $.ajax({
          data:  parametros,
          url: "<?php echo base_url(); ?>dp/deletecategoria",
          type:  'post',
          beforeSend: function () {
           //
          },
          success:  function (data) {

              $("#categoria_"+data).remove();
              cant = parseInt($("#cant_categorias").text());
              cant = cant -1;
              $("#cant_categorias").text(cant);

              cantidadCat = $("#padre_categias li").length;
              var url = "<?php echo base_url(); ?>dp/adm/categorias"; 
              if (cantidadCat < 1) { $(location).attr('href',url); }
    
          }
        });
      }

      function editarCAT($idCat) {


        nombre_editado = $("#nombre_categoria_"+$idCat).text();

        $('#categoria_'+$idCat).replaceWith(
          '<form action="<?php echo base_url(); ?>dp/updatacategoria" method="post" id="categoria_'+$idCat+'">'+
          '<div class="input-group" style="padding: 10px;">'+
            '<input type="text" class="form-control" name="nombre_categoria" id="categoria_intput_'+$idCat+'" required>'+
            '<input type="hidden" class="form-control" name="id_categoria" value="'+$idCat+'">'+
            '<span class="input-group-btn">'+
              '<button class="btn btn-default" type="submit">'+
                '<i class="fa fa-check" aria-hidden="true"></i></button>'+
              '<button class="btn btn-default" type="button" onClick="noeditarCAT('+$idCat+',\''+nombre_editado+'\')">'+
                '<i class="fa fa-times" aria-hidden="true"></i></button>'+
            '</span>'+
          '</div>'+
          '</form>');
        $('#categoria_intput_'+$idCat).focus();
      }

      function noeditarCAT($idCat,$nombreCat) {
        $('#categoria_'+$idCat).replaceWith(
          '<li class="list-group-item clearfix" id="categoria_'+$idCat+'">'+
            '<span id="nombre_categoria_'+$idCat+'">'+$nombreCat+'</span>'+
            '<div class="btn-group pull-right">'+
              '<button type="button" class="btn btn-default" onclick="editarCAT('+$idCat+');">'+
                '<i class="fa fa-pencil" aria-hidden="true"></i>'+
              '</button>'+
              '<button type="button" class="btn btn-default" onclick="elimiaCAT('+$idCat+');">'+
                '<i class="fa fa-trash-o" aria-hidden="true"></i>'+
              '</button>'+
            '</div>'+
          '</li>');
      }

      /*
      $(document).ready(function(){

      });
      */
    </script>


  </div>



  <div class="col-md-5">
    <div class="panel panel-default">
      <div class="panel-heading"><strong>AGREGAR CATEGORIA</strong></div>

      <div class="panel-body">
    

        <form class="form-inline" action="<?php echo base_url(); ?>dp/upcategoria" method="post">
          <div class="form-group">
            <input type="text" class="form-control" name="categoria" placeholder="Escriba la categoria" required>
          </div>
          <button type="submit" class="btn btn-default">Agregar categoria</button>
        </form>


      </div>
  
    </div>
  </div>



