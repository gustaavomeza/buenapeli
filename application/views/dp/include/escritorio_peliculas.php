  


  <div class="col-md-2">
    <div class="panel panel-default">
      <div class="panel-heading"><strong>OPCIONES</strong></div>

   
     
      <div class="list-group">

          <a href="<?php echo base_url(); ?>dp/adm/categorias" class="list-group-item">
            CATEGORIAS
            <span class="badge badge-default badge-pill" id="cant_categorias"><?php echo $numero_categorias; ?></span>
          </a>
          <a href="<?php echo base_url(); ?>dp/adm/peliculas" class="list-group-item">
            PELICULAS
            <span class="badge badge-default badge-pill" id="cant_peliculas"><?php echo $numero_peliculas; ?></span>
          </a>

      </div>

    </div>
  </div>

  <div class="col-md-5">


    


    <div class="panel panel-default">
    
      <div class="panel-heading"><strong>EDITAR PELICULAS</strong></div>
    
    
      <ul class="list-group" id="padre_categias">

        <?php foreach($lista as $fila) : 
        echo '
          <li class="list-group-item clearfix" id="pelicula_'.$fila->id_pelicula.'">
            <span id="nombre_pelicula_'.$fila->id_pelicula.'">'.$fila->nombre_pelicula.'</span>
            <div class="btn-group pull-right">
              <button type="button" class="btn btn-default" onClick="#">
                <i class="fa fa-pencil" aria-hidden="true"></i>
              </button>
              <button type="button" class="btn btn-default" onClick="eliminaPELI('.$fila->id_pelicula.', \''.$fila->imagen_pelicula.'\');">
                <i class="fa fa-trash-o" aria-hidden="true"></i>
              </button>
            </div>
          </li>
        ';

        endforeach;
        if ($numero_peliculas < 1) {
          echo '
          <li class="list-group-item clearfix">No existen peliculas</li>
          ';
        }
        ?>
        
      </ul>
      <!--<div class="panel-body"> </div>-->
      <div class="panel-footer">
        <?php echo $paginacion ?>
      </div>



      

    </div>
    <script type="text/javascript">

      function eliminaPELI($idPeli, $rutaImg) {
        parametros = {
          "idPeli" : $idPeli,"rutaImg" : $rutaImg,
        };
        $.ajax({
          data:  parametros,
          url: "<?php echo base_url(); ?>dp/deletepelicula",
          type:  'post',
          beforeSend: function () {
           //
          },
          success:  function (data) {

              $("#pelicula_"+data).remove();
              cant = parseInt($("#cant_peliculas").text());
              cant = cant -1;
              $("#cant_peliculas").text(cant);

              cantidadCat = $("#padre_categias li").length;
              var url = "<?php echo base_url(); ?>dp/adm/peliculas"; 
              if (cantidadCat < 1) { $(location).attr('href',url); }
    
          }
        });
      }

    

      /*
      $(document).ready(function(){

      });
      */
    </script>


  </div>



  <div class="col-md-5">
    <div class="panel panel-default">
      <div class="panel-heading"><strong>AGREGAR PELICULA</strong></div>

      <div class="panel-body">
    

        <form action="<?php echo base_url(); ?>dp/uppelicula" method="post" enctype="multipart/form-data">



          <div class="input-group">
            <span class="input-group-addon">Titulo</span>
            <input type="text" name="titulo_peli" class="form-control">
          </div>
          <small class="form-text text-muted">Titulo de la pelicula a agregar.</small><br><br>

  

          <div class="form-group">
            <div class='input-group date' id='datetimepicker8'>
              <span class="input-group-addon">Fecha</span>
              <input type='text' name="fecha_peli" class="form-control" />
              <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
            </div>
            <small class="form-text text-muted">Fecha de la pelicula.</small>
          </div>
          <script type="text/javascript">
              $(document).ready( function() {
                  $('#datetimepicker8').datetimepicker({
                   viewMode: 'years',
                   format: 'YYYY/MM/DD'
                  });
              });
          </script>


          <div>
          <?php 
          foreach($categorias as $fila) : 
            echo 
            '<span class="badge badge-default bagPOPcat" data-idpeli="'.$fila->id_categoria.'" data-estado="i"
                   onClick="activaCatpeli(this);">
              '.$fila->nombre_categoria.
            '</span>';

          endforeach;
          ?>
          <input type="hidden" id="bagsCAT" name="categorias_peli" value="">
          </div><small class="form-text text-muted">Seleccione sus categorias.</small><br><br>
          <style type="text/css">
            .bagPOPcat {
              cursor: pointer;
            }
            .bagActive {
              background: red !important;
            }
          </style>
          <script type="text/javascript">
            function activaCatpeli($this){

              if ($($this).data("estado") == 'i') {
                texto = $($this).text();
                idBag = $($this).data("idpeli");
                $($this).replaceWith(
                  '<span class="badge badge-default bagPOPcat bagActive" data-idpeli="'+idBag+'" data-estado="e"'+
                   'onclick="activaCatpeli(this);">'+
                  ''+texto+'</span>');

                acval = $("#bagsCAT").val();
                $("#bagsCAT").val(acval+'-'+idBag);
            


              } else {  
                texto = $($this).text();
                idBag = $($this).data("idpeli");
                $($this).replaceWith(
                  '<span class="badge badge-default bagPOPcat" data-idpeli="'+idBag+'" data-estado="i"'+
                   'onclick="activaCatpeli(this);">'+
                  ''+texto+'</span>');

                str = $("#bagsCAT").val();
                res = str.replace('-'+idBag, "");
                $("#bagsCAT").val(res);
          
              }
              //alert($($this).data("idpeli"));

            }
          </script>

          <div class="input-group">
            <span class="input-group-addon">Actores</span>
            <input type="text" name="actores_peli" class="form-control">
          </div>
          <small class="form-text text-muted">Actores de la pelicula.</small><br><br>











          <div class="row">
            <div class="col-md-6">

  
       
              <label class="btn btn-default btn-file btn-block">
                Cargar Portada <input type="file" name="mi_archivo" id="portadaPeli" style="display: none;">
              </label>
              <span id="imgen_pelicula_content"><br></span>

              <input type="hidden" name="x1" id="x1" size="4" value="0">
              <input type="hidden" name="y1" id="y1" size="4" value="0">
              <input type="hidden" name="x2" id="x2" size="4" value="60">
              <input type="hidden" name="y2" id="y2" size="4" value="90">
              <input type="hidden" name="ancho" id="ancho" size="4" value="60">
              <input type="hidden" name="alto" id="alto" size="4" value="90">



              <script type="text/javascript">
                $(document).ready( function() {
                    $("#portadaPeli").change(function(e){


                      imagen = $("#portadaPeli").val();
                      var TmpPath = URL.createObjectURL(e.target.files[0]);
                      $("#mi_imagen").attr("src", TmpPath);

                   
                      $("#imgen_pelicula_content").replaceWith(
                        '<span id="imgen_pelicula_content"><br><img id="duck" src="'+TmpPath+'" class="img-thumbnail" style="max-width: 100%;"></span>');

                      $('.imgareaselect-outer').remove();
                      $('.imgareaselect-selection').remove();
                      $('.imgareaselect-border1').parent().remove();

                 

                      $('#duck').imgAreaSelect({

                        x1: 0, y1: 0, x2: 60, y2: 90,
                        minHeight : 90,
                        minWidth : 60,
                        persistent : true,

                        aspectRatio : '3:4.5',
                        handles : true, 
                        onSelectEnd: function(img, sel){
                          $('#x1').prop('value', sel.x1);
                          $('#y1').prop('value', sel.y1);
                          $('#x2').prop('value', sel.x2);
                          $('#y2').prop('value', sel.y2);
                          $('#ancho').prop('value', sel.width);
                          $('#alto').prop('value', sel.height);
                        }
                      });



                    });
                });
              </script>



            </div>


            <div class="col-md-6">
              <button type="button" class="btn btn-default btn-block" id="agregar_video">
                Agregar Video <i class="fa fa-plus-circle" aria-hidden="true"></i>
              </button><br>
              <div id="items_videos"></div>        
            </div>


              


            <script type="text/javascript">



              function darVideosFinales() {

            
                objJSON = $('* .ordenCOP').map(function() {
                  var cadena = $( this ).val();
                  return cadena; 
                }).get().join( "|" );

                $('#todVideo').val(objJSON);
              }


              function darVal($num) {
                idioma = $('#idiomaa_car_'+$num).val();
                frame = $('#frame_car_'+$num).val();
                cadena = idioma+','+frame;
                $('#mitemp_'+$num).val(cadena);
                darVideosFinales();
              }





              function elimnarVideo($video) {
                $('#'+$video).remove();
                darVideosFinales();
              }



              $(document).ready( function() {

                $('#agregar_video').on( "click", function() {

                  ivideo = $('#items_videos .panel').length;

                  if (ivideo < 1) {

                    $('#items_videos').append(
                      '<div class="panel panel-default" id="video_1">'+
                        '<div class="panel-body">'+
                          '<div class="input-group">'+
                            '<span class="input-group-addon">Idioma</span>'+
                            '<input type="text" id="idiomaa_car_1" class="form-control" onClick="darVal(1)" onchange="darVal(1)" >'+
                            '<span class="input-group-btn"><button onClick="elimnarVideo(\'video_1\');" class="btn btn-default" type="button"><i class="fa   fa-times" aria-hidden="true"></i></button></span>'+
                          '</div><br>'+
                          '<input type="text" id="frame_car_1" class="form-control" placeholder="Ruta de pelicula" onClick="darVal(1)" onchange="darVal(1)">'+
                          '<input type = "hidden" class="ordenCOP" id = "mitemp_1" >'+
                        '</div>'+
                      '</div>');

                  } else {

                    ivideo = ivideo + 1;

                    $('#items_videos').append(
                      '<div class="panel panel-default" id="video_'+ivideo+'">'+
                        '<div class="panel-body">'+
                          '<div class="input-group">'+
                            '<span class="input-group-addon">Idioma</span>'+
                            '<input type="text" id="idiomaa_car_'+ivideo+'" class="form-control" onClick="darVal('+ivideo+')" onchange="darVal('+ivideo+')" >'+
                            '<span class="input-group-btn"><button onClick="elimnarVideo(\'video_'+ivideo+'\');" class="btn btn-default" type="button"><i class="fa   fa-times" aria-hidden="true"></i></button></span>'+
                          '</div><br>'+
                          '<input type="text" id="frame_car_'+ivideo+'" class="form-control" placeholder="Ruta de pelicula" onClick="darVal('+ivideo+')" onchange="darVal('+ivideo+')">'+
                          '<input type = "hidden" class="ordenCOP" id = "mitemp_'+ivideo+'" >'+
                        '</div>'+
                      '</div>');


                  }

                  darVideosFinales();




                });

              });
            </script>

            <input type="hidden" id="todVideo" name="videos_pelis">



          </div><br>









          <div class="form-group">
            <textarea class="form-control" rows="5" id="comment" name="coment_peli" style="resize: vertical; min-height: 100px;"></textarea>
            <small class="form-text text-muted">Descripccion de la pelicula.</small>
          </div>
          

          <button type="submit" class="btn btn-default">Agregar</button>
        </form>


      </div>
  
    </div>
  </div>



