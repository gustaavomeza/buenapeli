


<div class="container"><br>


  <div class="col-md-2"><br>
    <div class="panel panel-default">
      <div class="panel-heading"><strong>CATEGORIAS</strong></div>
  
      <div class="list-group">
        <?php 
        foreach($categorias as $fila) :
          echo '<a href="#" class="list-group-item">'.$fila->nombre_categoria.'</a>';
        endforeach;
        ?>
      </div>

    </div>
  </div>


  <div class="col-md-10"><br>
    <div class="panel panel-default">
      <div class="panel-heading"><strong>PELICULAS</strong></div>
  
      
        <?php foreach($lista as $fila) : 
        echo '

        <div class="col-md-3"><br>
          <div class="thumbnail">

            <img src="'.base_url().'/'.$fila->imagen_pelicula.'"
                 data-container="body" data-toggle="popover" data-placement="top"
                 data-trigger="hover" title="'.$fila->nombre_pelicula.'"
                 data-content="'.$fila->descripcion_pelicula.'"/>



            <div class="caption">
              <h4>'.$fila->nombre_pelicula.'</h4>
              <p><i class="fa fa-eye" aria-hidden="true"></i> '.$fila->reproducciones.'</p>

              <p><a href="#" class="btn btn-primary" role="button">VER PELICULA</a></p>

            </div>
          </div>
        </div>
        
        ';
        endforeach;
        ?>



    <div class="clearfix"></div>
    </div>
  </div>


</div>

<script>
$(function () {
  $('[data-toggle="popover"]').popover()
})
</script>



<div class="container">
  <div class="container_wrap">

    <div class="content">


      <div class="box_1">
        <h1 class="m_2">categorias</h1>
        <div class="search">
          <form>
            <input style="width: 300px;" type="text" 
                                         value="Buscar..." 
                                         onfocus="this.value='';"
                                         onblur="if (this.value == '') {this.value ='';}">
            <input type="submit" value="">
          </form>
        </div>
        <div class="clearfix"> </div>
      </div>


      <div class="box_2">


        <div class="row">

          <div class="col-md-3">
          

          </div>
          <div class="col-md-9">
            
            <?php foreach($lista as $fila) : 
            echo '
            <div class="col-md-3">
              <div class="col_2">
                <ul class="list_4">
                  <li style="line-height: 1.1em;">'.$fila->nombre_pelicula.'</li>
                  <li>Visto: '.$fila->reproducciones.'</li>
                  <div class="clearfix"> </div>
                </ul>
                <div class="m_5">
                  <a href="single.html">
                  <img src="'.base_url().'/'.$fila->imagen_pelicula.'" class="img-responsive" alt=""/>
                  </a>
                </div>
              </div>
            </div>  
            ';
            endforeach;
            ?>
  
          </div>
          <div class="col-md-3"></div>
          <div class="col-md-9">

            <?php echo $paginacion ?>
            
          </div>

        <div class="clearfix"> </div>
        </div>











      <div class="clearfix"> </div>
    </div>
      </div>
      </div>
      </div>