<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frases_modelo extends CI_Model {

    
    
    
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
    public function getNumFrases()
    {
        $this->db->distinct();
        $this->db->group_by("nombre_pelicula");
        $this->db->select('id_pelicula');
        $this->db->from('dp_peliculas');
        //$this->db->where('estatus',1);
        $num = $this->db->count_all_results();
    
        
        return $num;
    }
    
    
    public function getTodasFrases($limit,$start)
    {
        $this->db->distinct();
        $this->db->group_by("nombre_pelicula");
        $this->db->limit($limit,$start);
        $this->db->select('*');
        $this->db->from('dp_peliculas');
        //$this->db->where('estatus',1);
        //$this->db->order_by('nombre_pelicula', 'RANDOM');
        $this->db->order_by('id_pelicula', 'DESC');
        $resultado = $this->db->get();
    
        return $resultado->result();
    }
    
    

    public function getCategos()
    {

        $this->load->helper('global');

        $this->db->distinct();
        $this->db->group_by("genero");
        $this->db->select('UPPER(genero) as categoria_pelicula');
        $this->db->from('dp_peliculas');
        $get = $this->db->get();
        $resultado = $get->result();


        $dat = [];

        foreach ($resultado as $clave => $valor) {

            $valor = get_object_vars($valor);
            $valor = $valor['categoria_pelicula'];
            $arr = explode(',',$valor);
    
            foreach ($arr as $clave => $valor) {
                $dat[eliminar_acentos(trim($valor))]= trim($valor);
            }
 
            
        }


        return $dat;
    }








    public function getNumFrases2($serch)
    {

        $this->db->distinct();
        $this->db->group_by("nombre_pelicula");
        $this->db->select('id_pelicula');
        $this->db->from('dp_peliculas');
        $this->db->like('nombre_pelicula', $serch);
        //$this->db->where('estatus',1);
        $num = $this->db->count_all_results();
        
        
        return $num;
    }
    
    
    public function getTodasFrases2($limit,$start,$serch)
    {
        $this->db->distinct();
        $this->db->group_by("nombre_pelicula");
        $this->db->limit($limit,$start);
        $this->db->select('*');
        $this->db->from('dp_peliculas');
        $this->db->like('nombre_pelicula', $serch);
        //$this->db->where('estatus',1);
        //$this->db->order_by('nombre_pelicula', 'RANDOM');
        $this->db->order_by('id_pelicula', 'DESC');
        $resultado = $this->db->get();
    
        return $resultado->result();
    }
    






    public function getNumFrases3($cat)
    {
        $this->db->distinct();
        $this->db->group_by("nombre_pelicula");
        $this->db->select('id_pelicula');
        $this->db->from('dp_peliculas');
        $this->db->like('genero', $cat);
        //$this->db->where('estatus',1);
        $num = $this->db->count_all_results();
        
        
        return $num;
    }
    
    
    public function getTodasFrases3($limit,$start,$cat)
    {

        $this->load->helper('global');
        
        $cat = Rurl($cat,2);
      
        $this->db->distinct();
        $this->db->group_by("nombre_pelicula");
        $this->db->limit($limit,$start);
        $this->db->select('*');
        $this->db->from('dp_peliculas');
        $this->db->like('genero',rawurldecode($cat));
        //$this->db->where('estatus',1);
        //$this->db->order_by('nombre_pelicula', 'RANDOM');
        $this->db->order_by('id_pelicula', 'DESC');
        $resultado = $this->db->get();
    
        return $resultado->result();
    }
    

    


    
    
}


