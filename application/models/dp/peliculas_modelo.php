<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peliculas_modelo extends CI_Model {



private $tb_pelicluas = 'dp_peliculas';

function __construct()
{
    parent::__construct();
    $this->load->database();
}

public function getNumPeliculas()
{
    return $this->db->count_all($this->tb_pelicluas);
}


public function getTodasPeliculas($limit,$start)
{
    $this->db->limit($limit,$start);
    $resultado = $this->db->get($this->tb_pelicluas);

    return $resultado->result();
}


}


