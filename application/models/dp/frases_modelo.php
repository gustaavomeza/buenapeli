<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frases_modelo extends CI_Model {







private $tb_frases = 'dp_categorias';

function __construct()
{
    parent::__construct();
    $this->load->database();
}

public function getNumFrases()
{
    return $this->db->count_all($this->tb_frases);
}


public function getTodasFrases($limit,$start)
{
    $this->db->limit($limit,$start);
    $resultado = $this->db->get($this->tb_frases);

    return $resultado->result();
}

/*
        public function datos(){


            $this->db->select('*');
            $this->db->from('dp_estrenos');
            $query = $this->db->get();
            if($query->num_rows() > 0 ) {
                return $query->result_array();
            }


        }
*/

}


