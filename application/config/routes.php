<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$route['default_controller'] = 'welcome';
$route['default_controller'] = 'Inicio';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;



$route['inicio/(:num)'] = 'inicio';

$route['categoria/(:any)'] = 'cat';
$route['categoria/(:any)/(:num)'] = 'cat';

$route['pelicula/(:any)'] = 'pelicula';
$route['pelicula/(:any)/(:num)'] = 'pelicula';


$route['verpeli/(:any)/(:num)'] = 'verpeli';

$route['dp/adm/categorias/(:num)'] = 'dp/adm/categorias';
$route['dp/adm/peliculas/(:num)'] = 'dp/adm/peliculas';



$route['admin/page/(:any)'] = 'admin/page';