<?php



function Rurl($cad,$num){

    if($num==1){
        $cad = str_replace("(", "#01#", $cad);
        $cad = str_replace(")", "#02#", $cad);
        $cad = str_replace("+", "#03#", $cad);
        return $cad;
    }
    if($num==2){
        $cad = str_replace("#01#","(", $cad);
        $cad = str_replace("#02#",")", $cad);
        $cad = str_replace("#03#","+", $cad);
        return $cad;
    }

}




function gen_url($url){


   $url = strtolower($url);
   //Reemplazamos caracteres especiales latinos
   $find = array('á','é','í','ó','ú','â','ê','î','ô','û','ã','õ','ç','ñ');
   $repl = array('a','e','i','o','u','a','e','i','o','u','a','o','c','n');
   $url = str_replace($find, $repl, $url);
   //Añadimos los guiones
   $find = array(' ', '&', '\r\n', '\n','+');
   $url = str_replace($find, '-', $url);
   //Eliminamos y Reemplazamos los demas caracteres especiales
   $find = array('/[^a-z0-9\-<>]/', '/[\-]+/', '/<{^>*>/');
   $repl = array('', '-', '');
   $url = preg_replace($find, $repl, $url);
   return $url;


}



function eliminar_acentos($cadena){
		
    $cadena = str_replace(
    array('Á', 'À', 'Â', 'Ä', 'á', 'à', 'ä', 'â', 'ª'),
    array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a'),
    $cadena
    );

    //Reemplazamos la E y e
    $cadena = str_replace(
    array('É', 'È', 'Ê', 'Ë', 'é', 'è', 'ë', 'ê'),
    array('E', 'E', 'E', 'E', 'e', 'e', 'e', 'e'),
    $cadena );

    //Reemplazamos la I y i
    $cadena = str_replace(
    array('Í', 'Ì', 'Ï', 'Î', 'í', 'ì', 'ï', 'î'),
    array('I', 'I', 'I', 'I', 'i', 'i', 'i', 'i'),
    $cadena );

    //Reemplazamos la O y o
    $cadena = str_replace(
    array('Ó', 'Ò', 'Ö', 'Ô', 'ó', 'ò', 'ö', 'ô'),
    array('O', 'O', 'O', 'O', 'o', 'o', 'o', 'o'),
    $cadena );

    //Reemplazamos la U y u
    $cadena = str_replace(
    array('Ú', 'Ù', 'Û', 'Ü', 'ú', 'ù', 'ü', 'û'),
    array('U', 'U', 'U', 'U', 'u', 'u', 'u', 'u'),
    $cadena );

    //Reemplazamos la N, n, C y c
    $cadena = str_replace(
    array('Ñ', 'ñ', 'Ç', 'ç'),
    array('N', 'n', 'C', 'c'),
    $cadena
    );
    
    return $cadena;
}