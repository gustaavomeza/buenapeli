<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adm extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function index()
	{
       $this->load->view('dp/include/heder_dentro');


       $data['numero_categorias'] = $this->db->count_all_results('dp_categorias');
       $data['numero_peliculas'] = $this->db->count_all_results('dp_peliculas');
       $this->load->view('dp/include/escritorio', $data);


       $this->load->view('dp/include/foter');
	}



       public function categorias()
       {
       $this->load->view('dp/include/heder_dentro');

       $data['numero_categorias'] = $this->db->count_all_results('dp_categorias');
       $data['numero_peliculas'] = $this->db->count_all_results('dp_peliculas');


       $this->load->model('dp/frases_modelo');
       $this->load->library('pagination');

       $desde = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
       $opciones['per_page'] = 10;
       $opciones['base_url'] = base_url().'/dp/adm/categorias/';


       $opciones["total_rows"] = $this->frases_modelo->getNumFrases();

       //$opciones["use_page_numbers"] = true;
       $opciones["num_links"] = 4;

       $opciones["first_link"] = "priemra";
       $opciones["first_tag_open"] = "<li>";
       $opciones["first_tag_close"] = "</li>";

       $opciones["last_link"] = "ultima";
       $opciones["last_tag_open"] = "<li>";
       $opciones["last_tag_close"] = "</li>";

       $opciones["full_tag_open"] = '<ul class="pagination" style="margin:0px;">';
       $opciones["full_tag_close"] = '</div>';

       $opciones["next_link"] = '<i class="fa fa-arrow-right" aria-hidden="true"></i>';

       $opciones["next_tag_open"] = "<li>";
       $opciones["next_tag_close"] = "</li>";

       $opciones["prev_link"] = '<i class="fa fa-arrow-left" aria-hidden="true"></i>';

       $opciones["prev_tag_open"] = "<li>";
       $opciones["prev_tag_close"] = "</li>";

       $opciones["num_tag_open"] = '<li>';
       $opciones["num_tag_close"] = "</li>";

       $opciones['cur_tag_open'] = '<li class="active"><a href="#">';
       $opciones['cur_tag_close'] = '</a></li>';

       $opciones['uri_segment'] = 4;

       $this->pagination->initialize($opciones);

       $data['lista'] = $this->frases_modelo->getTodasFrases($opciones['per_page'],$desde);
       $data['paginacion'] = $this->pagination->create_links();


       $this->load->view('dp/include/escritorio_categorias', $data);


       $this->load->view('dp/include/foter');
       }





       public function peliculas()
       {
       $this->load->view('dp/include/heder_dentro');

       $data['numero_categorias'] = $this->db->count_all_results('dp_categorias');
       $data['numero_peliculas'] = $this->db->count_all_results('dp_peliculas');
       $this->load->model('dp/peliculas_modelo');
       $this->load->library('pagination');
       $desde = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
       $opciones['per_page'] = 10;
       $opciones['base_url'] = base_url().'/dp/adm/peliculas/';
       $opciones["total_rows"] = $this->peliculas_modelo->getNumPeliculas();
       //$opciones["use_page_numbers"] = true;
       $opciones["num_links"] = 4;
       $opciones["first_link"] = "priemra";
       $opciones["first_tag_open"] = "<li>";
       $opciones["first_tag_close"] = "</li>";
       $opciones["last_link"] = "ultima";
       $opciones["last_tag_open"] = "<li>";
       $opciones["last_tag_close"] = "</li>";
       $opciones["full_tag_open"] = '<ul class="pagination" style="margin:0px;">';
       $opciones["full_tag_close"] = '</div>';
       $opciones["next_link"] = '<i class="fa fa-arrow-right" aria-hidden="true"></i>';
       $opciones["next_tag_open"] = "<li>";
       $opciones["next_tag_close"] = "</li>";
       $opciones["prev_link"] = '<i class="fa fa-arrow-left" aria-hidden="true"></i>';
       $opciones["prev_tag_open"] = "<li>";
       $opciones["prev_tag_close"] = "</li>";
       $opciones["num_tag_open"] = '<li>';
       $opciones["num_tag_close"] = "</li>";
       $opciones['cur_tag_open'] = '<li class="active"><a href="#">';
       $opciones['cur_tag_close'] = '</a></li>';
       $opciones['uri_segment'] = 4;
       $this->pagination->initialize($opciones);
       $data['lista'] = $this->peliculas_modelo->getTodasPeliculas($opciones['per_page'],$desde);
       $data['paginacion'] = $this->pagination->create_links();



       $this->db->select('*');
       $query = $this->db->get('dp_categorias');
       $resultado = $query->result();
       $data['categorias'] = $resultado;


       $this->load->view('dp/include/escritorio_peliculas', $data);

       $this->load->view('dp/include/foter');
       }








}
