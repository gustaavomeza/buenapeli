<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upcategoria extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function index()
	{
              if (isset($_REQUEST['categoria'])) {
                     if ($_REQUEST['categoria'] != '') {

                            $data = array(
                                    'nombre_categoria' => $_REQUEST['categoria'],
                                    'ranking_categoria' => 0
                            );
                            


                     
                            $this->db->from('dp_categorias')->where('nombre_categoria', $_REQUEST['categoria']); 
                            $count = $this->db->count_all_results();  

                            if ($count < 1) {
                                   $this->db->insert('dp_categorias', $data);
                            }

                            redirect(base_url().'dp/adm/categorias'); 


                     }else {
                            redirect(base_url().'dp/adm/categorias'); 
                     }
               
              }else {
                     redirect(base_url().'dp/adm/categorias');
              }
       }


}
