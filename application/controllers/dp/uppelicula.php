<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Uppelicula extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function index()

  {

  $titulo_peli =  $_REQUEST['titulo_peli'];
  $fecha_peli =  $_REQUEST['fecha_peli'];
  $categorias_peli =  substr($_REQUEST['categorias_peli'], 1);
  $actores_peli =  $_REQUEST['actores_peli'];
  $x1 =  $_REQUEST['x1'];
  $y1 =  $_REQUEST['y1'];
  $x2 =  $_REQUEST['x2'];
  $y2 =  $_REQUEST['y2'];
  $anchura =  $_REQUEST['ancho'];
  $altura =  $_REQUEST['alto'];
  $videos_pelis =  $_REQUEST['videos_pelis'];
  $coment_peli =  $_REQUEST['coment_peli'];

 



  $this->db->where('nombre_pelicula', $titulo_peli);
  $this->db->from('dp_peliculas');
  $count = $this->db->count_all_results();
  
  if ($count < 1) {




  
    $mi_archivo = 'mi_archivo';
    $config['upload_path'] = "assets/images/portada/";
    $config['encrypt_name'] = TRUE;
    $config['allowed_types'] = "jpg";
    $config['max_size'] = "50000";
    $config['max_width'] = "2000";
    $config['max_height'] = "2000";
    $this->load->library('upload', $config);
    if (!$this->upload->do_upload($mi_archivo)) {
        //*** ocurrio un error
        $data['uploadError'] = $this->upload->display_errors();
        echo $this->upload->display_errors();
        return;
    }
    $data['uploadSuccess'] = $this->upload->data();
  
    $imagen = $this->upload->data( 'file_name' );
  
    $imagenDeOrigen = 'assets/images/portada/'.$imagen;
    $manejadorDeOrigen = imagecreatefromjpeg($imagenDeOrigen);
    $manejadorDeDestino = ImageCreateTrueColor($anchura, $altura);
    imagecopyresampled(
      $manejadorDeDestino,
      $manejadorDeOrigen,
      0,
      0,
      $x1,
      $y1,
      $anchura,
      $altura,
      $anchura,
      $altura
    );
    imagejpeg($manejadorDeDestino, 'assets/images/portada/'.$titulo_peli.'.jpg', 100);
    unlink('assets/images/portada/'.$imagen);
  
  


  
  
    $data = array(
                  'nombre_pelicula' => $titulo_peli,
                  'ano_pelicula' => $fecha_peli.' 00:00:00',
                  'mapeo_categorias' => $categorias_peli,
                  'actores' => $actores_peli,
                  'estrellas_pelicula' => 0,
                  'agrado_pelicula' => 0,
                  'desagrado_pelicula' => 0,
                  'reproducciones' => 0,
                  'id_comentarios_familia' => 0,
                  'descripcion_pelicula' => $coment_peli,
                  'imagen_pelicula' => 'assets/images/portada/'.$titulo_peli.'.jpg',
                  'id_videos_familia' => 0

    );
    $this->db->insert('dp_peliculas', $data);

    $id_pelicula = $this->db->insert_id();




    $data = array(
                  'id_comentarios_familia' => $id_pelicula,
                  'id_videos_familia' => $id_pelicula
    );

    $this->db->where('id_pelicula', $id_pelicula);
    $this->db->update('dp_peliculas', $data);


    




    $videos_pelis = explode ("|", $videos_pelis);
    $conteo = count($videos_pelis);
    
    for ($i = 0; $i< $conteo; $i++){
    
      $valor = $videos_pelis[$i];
  
  
      $idioma = strstr($valor, ',', true);

      
      $video = strstr($valor, ',');
      $video = substr($video, 1);

  
  
      $data = array(
                    'id_videos_familia' => $id_pelicula,
                    'idioma_video' => $idioma,
                    'estado_video' => 'bueno',
                    'enbed_video' => $video
  
      );
      $this->db->insert('dp_videos', $data);
    
    }


      
  


 


  
  }
  

  redirect(base_url().'dp/adm/peliculas'); 








  }




}
