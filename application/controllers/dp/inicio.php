<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function index()
	{
         $this->load->view('dp/include/heder');
         $this->load->view('dp/include/login');
         $this->load->view('dp/include/foter');
	}


       public function errorname()
       {
         $this->load->view('dp/include/heder');
         $this->load->view('dp/include/errorname');
         $this->load->view('dp/include/foter');
       }

       public function errorpass()
       {
         $this->load->view('dp/include/heder');
         $this->load->view('dp/include/errorpass');
         $this->load->view('dp/include/foter');
         
       }

       public function validardp()
       {
         $nombre = $_REQUEST['user'];
         $password = $_REQUEST['pass'];

         if ($nombre != 'asd') {
           redirect('dp/inicio/errorname');
         }

         if ($password != 'asd') {
           redirect('dp/inicio/errorpass');
         }
         redirect('dp/adm');

       }


}
