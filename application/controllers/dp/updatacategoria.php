<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Updatacategoria extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

  
	public function index()
	{
              if (isset($_REQUEST['nombre_categoria'])) {

                     if ($_REQUEST['nombre_categoria'] != '') {

              
                        $this->db->where('nombre_categoria', $_REQUEST['nombre_categoria']);
                        $this->db->from('dp_categorias');
                        $count = $this->db->count_all_results();

                        if ($count < 1) {


                          $data = array(
                                 'nombre_categoria' => $_REQUEST['nombre_categoria']
                          );
                          $this->db->where('id_categoria', $_REQUEST['id_categoria']);
                          $this->db->update('dp_categorias', $data);


                        }
                        redirect(base_url().'dp/adm/categorias'); 

                     }else {
                            redirect(base_url().'dp/adm/categorias'); 
                     }
               
              }else {
                     redirect(base_url().'dp/adm/categorias');
              }
       }


}
