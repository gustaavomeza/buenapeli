<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verpeli extends CI_Controller {


	public function index()
	{

      $this->load->view('web/include/heder');

      $this->load->model('web/frases_modelo');

    
      $id_pelicula = $this->uri->segment(3);



      $this->db->where('id_pelicula', $id_pelicula);
      $query = $this->db->get("dp_peliculas"); 
      $dataPages['pelicula'] = $query->result();






       $dataPages['lista_categorias'] = $this->frases_modelo->getCategos();


       
       $this->load->view('web/include/categos', $dataPages);
       $this->load->view('web/include/peli', $dataPages);


      $this->load->view('web/include/foter');


	}

}
