<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {



	function __construct()
	{
		parent::__construct();
		
	}

	public function index()
	{
              $this->load->helper('global');

              $this->load->view('admin/include/heder');



              $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 'nada';
              
              /*  PELICULAS */
              $query = 'SELECT nombre_pelicula, imagen_pelicula, id_pelicula, video_principal FROM dp_'.$page;
              $resultados = $this->db->query($query);
              $data['peliculas'] = $resultados->result();
              $data['page'] = $page;

              
 


              $this->load->view('admin/include/page',$data);
       
     
       
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect(base_url());
  }
  


}
