<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peliculas extends CI_Controller {



	function __construct()
	{
		parent::__construct();
		
	}

	public function index()
	{
              $this->load->helper('global');

              $this->load->view('admin/include/heder');



              
              
              /*  PELICULAS */
              $query = 'SELECT nombre_pelicula, imagen_pelicula, id_pelicula, video_principal FROM dp_peliculas';
              $resultados = $this->db->query($query);
              $data['peliculas'] = $resultados->result();

              
 


              $this->load->view('admin/include/peliculas',$data);
       
     
       
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect(base_url());
	}
  


}
