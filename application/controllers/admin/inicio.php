<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {



	function __construct()
	{
		parent::__construct();
		
		if(empty($_SESSION['id'])){
			if($_SESSION['tipo']!='a'){
				redirect(base_url());
			}
		}
  
		
	}

	public function index()
	{
              $this->load->view('admin/include/heder');
              $this->load->view('admin/include/dasbord');
       
       
       
       
       
       
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect(base_url());
	}

}
