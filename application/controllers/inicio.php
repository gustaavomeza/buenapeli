<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {



	public function index()
	{
              $this->load->view('web/include/heder');
       
       
       
       
              $this->load->model('web/frases_modelo');
              $this->load->library('pagination');
              $desde = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
              $opciones['per_page'] = 16;
              $opciones['base_url'] = base_url().'/inicio/';
              $opciones["total_rows"] = $this->frases_modelo->getNumFrases();
              $opciones["num_links"] = 4;
              $opciones["first_link"] = '<i class="zmdi zmdi-arrow-left"></i>';
              $opciones["first_tag_open"] = '<li class="page-item"><span class="page-link">';
              $opciones["first_tag_close"] = '</span></li>';
              $opciones["last_link"] = '<i class="zmdi zmdi-arrow-right"></i>';
              $opciones["last_tag_open"] = '<li class="page-item"><span class="page-link">';
              $opciones["last_tag_close"] = '</span></li>';
              $opciones["full_tag_open"] = '<nav aria-label="Page navigation example"><ul class="pagination pagination-circle pg-blue">';
              $opciones["full_tag_close"] = '</lu></nav>';
              $opciones["next_link"] = '<i class="zmdi zmdi-chevron-right"></i>';
              $opciones["next_tag_open"] = '<li class="page-item"><span class="page-link">';
              $opciones["next_tag_close"] = '</span></li>';
              $opciones["prev_link"] = '<i class="zmdi zmdi-chevron-left"></i>';
              $opciones["prev_tag_open"] = '<li class="page-item"><span class="page-link">';
              $opciones["prev_tag_close"] = '</span></li>';
              $opciones["num_tag_open"] = '<li class="page-item"><span class="page-link">';
              $opciones["num_tag_close"] = '</span></li>';
              $opciones["cur_tag_open"] = '<li class="page-item active"><span class="page-link">';
              $opciones["cur_tag_close"] = '</span></li>';
              $opciones['uri_segment'] = 2;
       
       
       
       
       
              $this->pagination->initialize($opciones);
       
              $dataPages['lista'] = $this->frases_modelo->getTodasFrases($opciones['per_page'],$desde);
              $dataPages['paginacion'] = $this->pagination->create_links();
       
       
       
              $dataPages['lista_categorias'] = $this->frases_modelo->getCategos();
              
       
       
              $this->load->view('web/include/categos', $dataPages);
              $this->load->view('web/include/pelis', $dataPages);
       
       
       
              $this->load->view('web/include/foter');
	}

}
